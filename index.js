import express from 'express'
import path from 'path'
import http from 'http'
import socketIO from 'socket.io'
import UserEvents from './events/user_events'

const app = express();
const server = http.createServer(app);
const io = socketIO(server)
const root = __dirname + '/views/'
const user_events = new UserEvents()

server.listen(3000, () => { console.log('Server open on 3000')})
app.use('/', express.static('views/'));

app.get('/', (req, res) => {
  res.sendFile('index.html')
})


io.on('connection', (socket) => {
  socket.emit("connected", { 
    message: `Connected to server.`,
    token: new_user.token, 
    username: new_user.username 
  })

  socket.on('create_user_request', user_events.create_user)

  socket.on('update_user_request', user_events.update_user.bind(socket, data))

  socket.on('disconnect', () => {
    console.log('Disconnected from server.')
  })
})