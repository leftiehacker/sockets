const socket = io();
const defineType = text => text.split(' ')[0]

function sendText () {
  const token = localStorage.getItem('token')
  const username = localStorage.getItem('username')

  socket.emit('update_user_request', {
    token,
    username,
    event: 'increment_attack',
  })
}

socket.on("connected", (data) => {
  console.log(data.message)
  localStorage.setItem('token', data.token)
  localStorage.setItem('username', data.username)
})

socket.on("update_user_error", (data) => {
  console.log(data.error)
})

socket.on("update_user_success", (data) => {
  console.log("user: ", data.user);
});