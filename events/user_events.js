import Users from '../models/users_model'

// replace this with JWT
function generate_token () {
  const number = () => Math.floor(Math.random() * 10)
  let token = ''

  for(let i = 0; i < 20; i += 1) {
    token.concat(number())
  }

  return token
}

function auth_user (token, username) {
  const user = Users.filter(u => token == u.token && username == u.username)

  return user.length > 1
  ? { error: 'error: token in use', success: false }
  : user.length < 1
  ? { error: 'error: token does not exist', success: false }
  : { user: user.shift(), success: true }
}

export default class UserEvents {
  create_user(data) {
    const new_user = { 
      _id: Users.length,
      username: data.username,
      level: 1,
      attack: 1, 
      shield: 1,
      intelligence: 1,
    }

    new_user.token = generate_token()
    Users.push(new_user)
  }

  update_user(data) {
    const { user, success, error } = auth_user()

    if (success) {
      UserEvents[data.event](user)
      this.emit('update_user_successful', { user })
    } else {
      this.emit('update_user_error', { error })
    }
  }

  static increment_attack(user) {
    return user.attack++
  }

  static decrement_attack(user) {
    return user.attack--
  }

  static increment_shield(user) {
    return user.shield++
  }

  static decrement_shield(user) {
    return user.shield--
  }

  static increment_intelligence(user) {
    return user.intelligence++
  }

  static decrement_intelligence(user) {
    return user.intelligence--
  }
}
